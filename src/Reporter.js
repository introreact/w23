import React from "react";

const Reporter = (props) => {
    return (
      <div>
        {props.name}: {props.children}
      </div>
    )
}

export default Reporter;
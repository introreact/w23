import './App.css';
import Reporter from './Reporter';

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <Reporter name={"Antero Mertaranta"}> 
          Löikö mörkö sisään 
          <img src="img/picture.jpg" className="pic" alt="img" />
        </Reporter>
        <Reporter name={"Kevin McGran"}> 
          I know it's a rough time now, but did you at least enjoy playing in the tournament
        </Reporter>
      </header>
    </div>
  );
}

export default App;
